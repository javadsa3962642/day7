import java.util.*;

class Solution1{
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter number of rows :");
		int rows = sc.nextInt();

		int x = 1; 
		char y= 'a';
		for ( int i = 1; i<=rows+1;i++){
			for (int j = 1; j<=rows;j++){
				if (i%2==1){
					System.out.print(x++  + " ");
				} else{
                                        System.out.print(y++  + " ");
				} 
			} 

			System.out.println();
		} 
	}
}
