import java.util.*;

class Solution3{
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter a number : ");
		int num= sc.nextInt();

		String str = String.valueOf(num);
		if(str.contains("0") && str.charAt(0) != '0'){
			System.out.println(num  + "is a Duck number");
		} else{
			System.out.println(num  + "is not a Duck number");
		}
	}
}
